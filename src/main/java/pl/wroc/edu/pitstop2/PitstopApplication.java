package pl.wroc.edu.pitstop2;

import android.app.Application;
import android.content.Context;

import com.google.common.eventbus.EventBus;

/**
 * @author Tomasz Trybała
 */
public class PitstopApplication extends Application {
    private static Context sContext;
    private static EventBus sBus;

    @Override
    public void onCreate() {
        super.onCreate();

        sContext = this;
        sBus = new EventBus("Sound Pranks Event Bus");
    }

    /**
     * Returns application context;
     *
     * @return context
     */
    public static Context getAppContext() {
        return sContext;
    }

    /**
     * Returns application event bus.
     *
     * @return application event bus
     */
    public static EventBus getApplicationEventBus() {
        return sBus;
    }
}

package pl.wroc.edu.pitstop2;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.common.eventbus.Subscribe;

import pl.wroc.edu.pitstop2.event.FragmentEvent;
import pl.wroc.edu.pitstop2.event.POIFoundEvent;
import pl.wroc.edu.pitstop2.event.PolylineEvent;
import pl.wroc.edu.pitstop2.fragments.ChoosePlaceFragment;
import pl.wroc.edu.pitstop2.fragments.TracesFragment;
import pl.wroc.edu.pitstop2.maps.Route;
import pl.wroc.edu.pitstop2.maps.Routing;
import pl.wroc.edu.pitstop2.maps.RoutingListener;

public class MainActivity extends ActionBarActivity implements RoutingListener {
    private LinearLayout mLlFind;
    private LinearLayout mLlBase;
    private LinearLayout mLlAdd;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar mToolbar;
    private FrameLayout mFlContainer;

    private boolean mIsDoubleClicked;
    private Routing mRouting;
    private boolean mIsTaskRunning;
    private LatLng mStart;
    private LatLng mEnd;

    private FragmentTransaction mFragmentTransaction;
    public static FragmentManager mFragmentManager;
    protected Fragment mCurrentFragment;
    protected ChoosePlaceFragment mChoosePlaceFragment;
    protected TracesFragment mTracesFragment;


    private void findViews() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mLlAdd = (LinearLayout) mDrawerLayout.findViewById(R.id.llDrawerAdd);
        mLlBase = (LinearLayout) mDrawerLayout.findViewById(R.id.llDrawerBase);
        mLlFind = (LinearLayout) mDrawerLayout.findViewById(R.id.llDrawerFind);
        mFlContainer = (FrameLayout) findViewById(R.id.container);
    }

    private void createFragments() {
        mChoosePlaceFragment = new ChoosePlaceFragment();
        mTracesFragment = new TracesFragment();

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager
                .beginTransaction()
                .add(R.id.container, mChoosePlaceFragment, "main fragment")
                .add(R.id.container, mTracesFragment, "profile fragment")
                .hide(mTracesFragment);
        mFragmentTransaction.commit();

        setFragment(MainType.CHOOSE_PLACE);
    }

    private void setFragment(MainType type) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (type) {
            case CHOOSE_PLACE:
                if (mCurrentFragment != mChoosePlaceFragment) {
                    fragmentManager.popBackStack();
                    mFragmentTransaction = fragmentManager.beginTransaction();
                    if (mCurrentFragment != null) {
                        mFragmentTransaction.hide(mCurrentFragment);
                    }
                    mFragmentTransaction.show(mCurrentFragment = mChoosePlaceFragment);
                    mFragmentTransaction.commit();
                }
                break;
            case POLYLINE:
                if (mCurrentFragment != mTracesFragment) {
                    fragmentManager.popBackStack();
                    mFragmentTransaction = fragmentManager.beginTransaction();
                    if (mCurrentFragment != null) {
                        mFragmentTransaction.hide(mCurrentFragment);
                    }
                    mFragmentTransaction.show(mCurrentFragment = mTracesFragment);
                    mFragmentTransaction.commit();
                }
                break;
        }
    }

    private void setListeners() {
        mLlAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddPlaceActivity.class);
                startActivity(intent);
            }
        });

        mLlBase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, PitstopActivity.class);
                startActivity(intent);
            }
        });

        mLlFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PitstopApplication.getApplicationEventBus().post(new FragmentEvent(MainType.CHOOSE_PLACE));
            }
        });
    }

    private void setDrawer() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.app_name, R.string.app_name);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.setDrawerTitle(Gravity.START, "Title");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();
        setListeners();
        setDrawer();
        createFragments();

        mRouting = new Routing(Routing.TravelMode.DRIVING);
        mRouting.registerListener(this);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setFragment(MainType.CHOOSE_PLACE);
                getSupportActionBar().setTitle(MainType.CHOOSE_PLACE.getLabel());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mCurrentFragment != mChoosePlaceFragment) {
            setFragment(MainType.CHOOSE_PLACE);
            getSupportActionBar().setTitle(MainType.CHOOSE_PLACE.getLabel());
        } else {
            if (mIsDoubleClicked) {
                super.onBackPressed();
            } else {
                mIsDoubleClicked = true;
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mIsDoubleClicked = false;
                    }
                }, 2000);
                Toast.makeText(getApplicationContext(),
                        getString(R.string.back_exit_message),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Subscribe
    public void update(final FragmentEvent event) {
        getSupportActionBar().setTitle(event.getType().getLabel());
        setFragment(event.getType());
        mDrawerLayout.closeDrawers();
    }

    public void setFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(mFlContainer.getId(), fragment);
        ft.addToBackStack("");
        ft.commit();
    }

    @Subscribe
    public void onSelectedTrace(final PolylineEvent event) {
        mStart = event.getStart();
        mEnd = event.getEnd();
        if (!mIsTaskRunning) {
            mRouting.execute(mStart, mEnd);
            mTracesFragment.setParams(event.getGap(), event.isDiner(), event.isAccommodation());
            mIsTaskRunning = true;
        } else {
            Toast.makeText(this, "Wyszukiwanie w toku", Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onPOIFound(final POIFoundEvent event) {
        mTracesFragment.setPitstops(event.getPitstops());
    }

    @Override
    protected void onStart() {
        super.onStart();
        PitstopApplication.getApplicationEventBus().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        PitstopApplication.getApplicationEventBus().unregister(this);
    }

    @Override
    public void onRoutingFailure() {
        mIsTaskRunning = false;
        mRouting = new Routing(Routing.TravelMode.DRIVING);
        mRouting.registerListener(this);
        Toast.makeText(this, "Nie odnaleziono trasy", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRoutingStart() {
    }

    @Override
    public void onRoutingSuccess(Route[] routes) {
        mIsTaskRunning = false;
        mRouting = new Routing(Routing.TravelMode.DRIVING);
        mRouting.registerListener(this);
        PitstopApplication.getApplicationEventBus().post(new FragmentEvent(MainType.POLYLINE));
        mTracesFragment.setPolylinesData(routes, mStart, mEnd);
    }
}

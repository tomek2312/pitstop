package pl.wroc.edu.pitstop2.event;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

import pl.wroc.edu.pitstop2.model.Pitstop;

/**
 *@author Jakub Skudlarski
 */
public class POIFoundEvent {

    private ArrayList<Pitstop> mPitstops;


    public POIFoundEvent(ArrayList<Pitstop> pitstops){
        mPitstops = pitstops;
    }

    public ArrayList<Pitstop> getPitstops(){
        return mPitstops;
    }
}

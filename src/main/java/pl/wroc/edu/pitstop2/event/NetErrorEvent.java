package pl.wroc.edu.pitstop2.event;

/**
 *@author Tomasz Trybała
 */
public class NetErrorEvent {
    private String mMessage;

    public NetErrorEvent(String message){
        mMessage = message;
    }

    public String getMessage(){return mMessage;}
}

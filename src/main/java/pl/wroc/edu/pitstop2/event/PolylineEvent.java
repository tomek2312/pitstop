package pl.wroc.edu.pitstop2.event;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

/**
 *@author Tomasz Trybała
 */
public class PolylineEvent {
    private PolylineOptions mPolyOptions;
    private LatLng mStart, mEnd;
    private int mGap;
    private boolean mIsDiner;
    private boolean mIsAccommodation;

    public PolylineEvent(PolylineOptions polyOptions, final LatLng start, final LatLng end, int gap,
                         boolean isDiner, boolean isAccommodation){
        mPolyOptions = polyOptions;
        mStart = start;
        mEnd = end;
        mGap = gap;
        mIsAccommodation = isAccommodation;
        mIsDiner = isDiner;
    }

    public PolylineOptions getPolylineOptions(){
        return mPolyOptions;
    }

    public LatLng getStart(){
        return mStart;
    }

    public LatLng getEnd(){
        return mEnd;
    }

    public int getGap(){return mGap;}

    public boolean isDiner(){return mIsDiner;}

    public boolean isAccommodation(){return mIsAccommodation;}
}

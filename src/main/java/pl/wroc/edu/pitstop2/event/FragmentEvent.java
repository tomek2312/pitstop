package pl.wroc.edu.pitstop2.event;


import pl.wroc.edu.pitstop2.MainType;

/**
 *@author Tomasz Trybała
 */
public class FragmentEvent {
    private MainType mType;
    private String mMoveType;
    private boolean mIsMove;

    public FragmentEvent(MainType type){
        mType = type;
        mIsMove = false;
    }

    public FragmentEvent(MainType type, String moveType){
        mType = type;
        mMoveType = moveType;
        mIsMove = true;
    }

    public MainType getType(){
        return mType;
    }

    public String getMoveType(){return mMoveType;}

    public boolean isMove(){
        return mIsMove;
    }
}

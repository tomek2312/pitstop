package pl.wroc.edu.pitstop2;

/**
 *@author Tomasz Trybała
 */
public final class BundleConstants {
    private BundleConstants(){}

    public static final String PITSTOP = "pitstop";
    public static final String IS_MAP = "is_map";
}

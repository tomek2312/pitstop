package pl.wroc.edu.pitstop2;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;

import pl.wroc.edu.pitstop2.fragments.AllPitstopsFragment;


/**
 *@author Tomasz Trybała
 */
public class PitstopActivity extends ActionBarActivity {
    private FrameLayout mFlContent;

    private void findViews(){
        mFlContent = (FrameLayout) findViewById(R.id.container);
    }

    @SuppressLint("InflateParams")
    private void prepareLayout() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void setFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(mFlContent.getId(), fragment);
        ft.addToBackStack("");
        ft.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pitstop);
        prepareLayout();
        findViews();
        setFragment(new AllPitstopsFragment());
    }

    protected void popBackStack() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 1) {
            fm.popBackStack();
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        popBackStack();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                popBackStack();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}

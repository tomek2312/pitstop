package pl.wroc.edu.pitstop2;


import android.annotation.SuppressLint;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import pl.wroc.edu.pitstop2.model.Pitstop;
import pl.wroc.edu.pitstop2.net.HttpPost;
import pl.wroc.edu.pitstop2.net.OnRequestFinishedListener;
import pl.wroc.edu.pitstop2.net.RequestExecutor;

/**
 * @author Jakub Skudlarski & Tomasz Trybała
 */
public class AddPlaceActivity extends ActionBarActivity {
    //Views
    private EditText mEdtName;
    private EditText mEdtInfo;
    private EditText mEdtLocation;
    private EditText mEdtPhone;
    private EditText mEdtUrl;
    private Spinner mSpinnerStartHours;
    private Spinner mSpinnerEndHours;
    private Spinner mSpinnerStartMinutes;
    private Spinner mSpinnerEndMinutes;
    private Spinner mSpinnerTypeMain;
    private Spinner mSpinnerTypDetail;
    private ImageButton mBtnFind;
    private Calendar mStartTime;
    private Calendar mEndTime;
    private MapView mMapView;
    private GoogleMap mMap;
    private View mSendView;
    private ScrollView mSvForm;
    private LinearLayout mLlLocation;
    private Button mBtnHide;

    private MarkerOptions mMarkerOptions = new MarkerOptions();
    private RequestExecutor mRequestExecutor = new RequestExecutor();
    private LatLng mLocation;
    private boolean mIsDiner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_place);
        init();
        findViews();
        initAdapters();
        addListeners();
        prepareLayout();
        setMenu();
        initMap(savedInstanceState);
    }

    private void initMap(Bundle savedInstanceState) {
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMap = mMapView.getMap();
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mLocation = latLng;
                mEdtLocation.setText("" + latLng.latitude + ", " + latLng.longitude);
                animateMap();
            }
        });

        mMarkerOptions = new MarkerOptions()
                .title("Pitstop")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pitstop_marker));
    }

    private void setMenu() {
        LayoutInflater inflater = LayoutInflater.from(this);
        mSendView = inflater.inflate(R.layout.view_menu_send, null, false);
        ImageView imgvMenuMessages = (ImageView) mSendView.findViewById(R.id.imgvMenuSend);

        imgvMenuMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkViews();
            }
        });
    }

    private void init() {
        mStartTime = Calendar.getInstance();
        mEndTime = Calendar.getInstance();
    }

    @SuppressLint("InflateParams")
    private void prepareLayout() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void findViews() {
        mEdtName = (EditText) findViewById(R.id.editTextPlaceName);
        mEdtInfo = (EditText) findViewById(R.id.editTextPlaceInfo);
        mEdtPhone = (EditText) findViewById(R.id.editTextPlacePhone);
        mEdtUrl = (EditText) findViewById(R.id.editTextPlaceUrl);
        mEdtLocation = (EditText) findViewById(R.id.editTextLocalization);
        mSpinnerStartHours = (Spinner) findViewById(R.id.spinnerStartHour);
        mSpinnerEndHours = (Spinner) findViewById(R.id.spinnerEndHour);
        mSpinnerStartMinutes = (Spinner) findViewById(R.id.spinnerStartMinutes);
        mSpinnerEndMinutes = (Spinner) findViewById(R.id.spinnerEndMinutes);
        mSpinnerTypeMain = (Spinner) findViewById(R.id.spinnerTypeMain);
        mSpinnerTypDetail = (Spinner) findViewById(R.id.spinnerTypeDetail);
        mBtnFind = (ImageButton) findViewById(R.id.buttonFindPlace);
        mMapView = (MapView) findViewById(R.id.mapView);
        mSvForm = (ScrollView) findViewById(R.id.svAddPlace);
        mLlLocation = (LinearLayout) findViewById(R.id.llAddPlaceLocation);
        mBtnHide = (Button) findViewById(R.id.btnAddPlaceChangeLayout);
    }

    private void initAdapters() {
        String[] mainTypes = getResources().getStringArray(R.array.type_main);
        List<String> mainTypesList = new ArrayList<>(Arrays.asList(mainTypes));
        ArrayAdapter<String> mainTypesAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, mainTypesList);
        mainTypesAdapter.setDropDownViewResource(R.layout.spinner_row);
        mSpinnerTypeMain.setAdapter(mainTypesAdapter);

        String[] hours = getResources().getStringArray(R.array.hours);
        List<String> hoursArrayList = new ArrayList<>(Arrays.asList(hours));
        String[] minutes = getResources().getStringArray(R.array.minutes);
        List<String> minutesArrayList = new ArrayList<>(Arrays.asList(minutes));
        ArrayAdapter<String> startHoursAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, hoursArrayList);
        ArrayAdapter<String> endHoursAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, hoursArrayList);
        ArrayAdapter<String> startMinutesAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, minutesArrayList);
        ArrayAdapter<String> endMinutesAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, minutesArrayList);
        startHoursAdapter.setDropDownViewResource(R.layout.spinner_row);
        mSpinnerStartHours.setAdapter(startHoursAdapter);
        endHoursAdapter.setDropDownViewResource(R.layout.spinner_row);
        mSpinnerEndHours.setAdapter(endHoursAdapter);
        startMinutesAdapter.setDropDownViewResource(R.layout.spinner_row);
        mSpinnerStartMinutes.setAdapter(startMinutesAdapter);
        endMinutesAdapter.setDropDownViewResource(R.layout.spinner_row);
        mSpinnerEndMinutes.setAdapter(endMinutesAdapter);

        mSpinnerStartHours.setSelection(startHoursAdapter.getPosition("08"), true);
        mSpinnerEndHours.setSelection(endHoursAdapter.getPosition("20"));
    }

    private void addListeners() {
        mBtnHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mSvForm.getVisibility() == View.GONE){
                    mSvForm.setVisibility(View.VISIBLE);
                    mLlLocation.setVisibility(View.GONE);
                }else{
                    mSvForm.setVisibility(View.GONE);
                    mLlLocation.setVisibility(View.VISIBLE);
                }
            }
        });

        mBtnFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mEdtLocation.getText())) {
                    LatLng position = getLocationFromAddress(mEdtLocation.getText().toString());
                    if (position != null) {
                        mLocation = position;
                        animateMap();
                    } else {
                        Toast.makeText(getApplicationContext(), "Nie znaleziono lokalizacji", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    mEdtLocation.setError("Wpisz lokalizację");
                }
            }
        });
        mSpinnerStartHours.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mStartTime.set(Calendar.HOUR, Integer.parseInt((parent.getItemAtPosition(position).toString())));
                Log.i(AddPlaceActivity.class.getName(), "start time:" + mStartTime.get(Calendar.HOUR) + ":" + mStartTime.get(Calendar.MINUTE));
                Log.i(AddPlaceActivity.class.getName(), "end time:" + mEndTime.get(Calendar.HOUR) + ":" + mEndTime.get(Calendar.MINUTE));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSpinnerStartMinutes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mStartTime.set(Calendar.MINUTE, Integer.parseInt((parent.getItemAtPosition(position).toString())));
                Log.i(AddPlaceActivity.class.getName(), "start time:" + mStartTime.get(Calendar.HOUR) + ":" + mStartTime.get(Calendar.MINUTE));
                Log.i(AddPlaceActivity.class.getName(), "end time:" + mEndTime.get(Calendar.HOUR) + ":" + mEndTime.get(Calendar.MINUTE));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mSpinnerEndHours.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mEndTime.set(Calendar.HOUR, Integer.parseInt((parent.getItemAtPosition(position).toString())));
                Log.i(AddPlaceActivity.class.getName(), "start time:" + mStartTime.get(Calendar.HOUR) + ":" + mStartTime.get(Calendar.MINUTE));
                Log.i(AddPlaceActivity.class.getName(), "end time:" + mEndTime.get(Calendar.HOUR) + ":" + mEndTime.get(Calendar.MINUTE));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSpinnerEndMinutes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mEndTime.set(Calendar.MINUTE, Integer.parseInt((parent.getItemAtPosition(position).toString())));
                Log.i(AddPlaceActivity.class.getName(), "start time:" + mStartTime.get(Calendar.HOUR) + ":" + mStartTime.get(Calendar.MINUTE));
                Log.i(AddPlaceActivity.class.getName(), "end time:" + mEndTime.get(Calendar.HOUR) + ":" + mEndTime.get(Calendar.MINUTE));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mSpinnerTypeMain.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String[] mainTypes;
                if(mSpinnerTypeMain.getSelectedItem().equals("Nocleg")){
                    mainTypes = getResources().getStringArray(R.array.type_detail_accommodation);
                    mIsDiner = false;

                }else{
                    mainTypes = getResources().getStringArray(R.array.type_detail_diner);
                    mIsDiner = true;
                }

                List<String> mainTypesList = new ArrayList<>(Arrays.asList(mainTypes));
                ArrayAdapter<String> mainTypesAdapter = new ArrayAdapter<>(getApplicationContext(),
                        android.R.layout.simple_spinner_item, mainTypesList);
                mainTypesAdapter.setDropDownViewResource(R.layout.spinner_row);
                mSpinnerTypDetail.setAdapter(mainTypesAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void checkViews() {
        if (!TextUtils.isEmpty(mEdtName.getText())) {
            if (!TextUtils.isEmpty(mEdtInfo.getText())) {
                if (!TextUtils.isEmpty(mEdtLocation.getText())) {
                    sendPostOnServer();
                } else {
                    mEdtLocation.setError("Wybierz lokalizację");
                }
            } else {
                mEdtInfo.setError("Wpisz informację");
            }
        } else {
            mEdtName.setError("Wpisz nazwę");
        }
    }

    private void sendPostOnServer() {
        final OnRequestFinishedListener<String> listener = new OnRequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(int responseCode, String message, boolean canceled) {
                String result;
                if (responseCode == 200) {
                    result = "Dodano Pitstop";
                } else {
                    Log.e("dupa", "message: "+message);
                    result = "Nie dodano Pitstopu!";
                }
                Toast.makeText(PitstopApplication.getAppContext(),
                        result,
                        Toast.LENGTH_SHORT).show();
            }
        };
        Pitstop pitstop = new Pitstop();
        pitstop.setName(mEdtName.getText().toString());
        pitstop.setDescription(mEdtInfo.getText().toString());
        pitstop.setLatitude(mLocation.latitude);
        pitstop.setLongitude(mLocation.longitude);
        pitstop.setUrl(mEdtUrl.getText().toString());
        pitstop.setPhone(mEdtPhone.getText().toString());
        pitstop.setTimeOpen( (mSpinnerStartHours.getSelectedItem() + ":" + mSpinnerStartMinutes.getSelectedItem()));
        pitstop.setTimeClose((mSpinnerEndHours.getSelectedItem() + ":" + mSpinnerEndMinutes.getSelectedItem()));
        pitstop.setType((String) mSpinnerTypDetail.getSelectedItem());

        if(mIsDiner){
            pitstop.setCouisine("Amerykanska");
            //todo: amerykańska, hiszpańska, polska, włoska
        }else{
            pitstop.setStars(1);
        }

        HttpPost<String> postRequest = new HttpPost<>(
                mRequestExecutor,
                false,
                mIsDiner ? getResources().getString(R.string.server_host_add_diner) :
                        getResources().getString(R.string.server_host_add_accommodation),
                pitstop,
                new HttpPost.ServerResponseParser<String>() {
                    @Override
                    public String parse(String message) {
                        return message;
                    }
                },
                listener
        );

        postRequest.executePitstop();
    }

    public LatLng getLocationFromAddress(String strAddress) {
        Geocoder geocoder = new Geocoder(this);
        List<Address> addresses;
        LatLng latLng;

        try {
            addresses = geocoder.getFromLocationName(strAddress, 5);

            if (addresses == null || addresses.isEmpty()) {
                return null;
            }

            Address location = addresses.get(0);
            latLng = new LatLng((location.getLatitude()),
                    (location.getLongitude()));

            return latLng;
        } catch (IOException e) {
            return null;
        }
    }

    private void animateMap() {
        mMap.clear();
        mMap.addMarker(mMarkerOptions.position(mLocation));

        if (mMap.getCameraPosition().zoom < 15) {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(mLocation).zoom(15).build();
            mMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my, menu);

        MenuItem messages = menu.findItem(R.id.action_send);
        messages.setActionView(mSendView);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

}

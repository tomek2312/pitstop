package pl.wroc.edu.pitstop2.fragments;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;

import pl.wroc.edu.pitstop2.PitstopApplication;
import pl.wroc.edu.pitstop2.R;
import pl.wroc.edu.pitstop2.event.PolylineEvent;


/**
 * @author Jakub Skudlarski
 */
public class ChoosePlaceFragment extends Fragment {

    //Views
    private TextView distanceValueTextView;
    private EditText startEditText;
    private EditText finishEditText;
    private CheckBox mCbAccommodation;
    private CheckBox mCbDiner;
    private SeekBar distanceSeekBar;
    private Button searchButton;
    private ImageButton locateButton;

    protected LatLng start;
    protected LatLng end;
    private double mLatitude;
    private double mLongitude;

    private boolean mIsYourLocation;
    private String mLastStartLocation;

    private void findViews(View view) {
        distanceValueTextView = (TextView) view.findViewById(R.id.textViewDistanceValue);
        startEditText = (EditText) view.findViewById(R.id.editTextStart);
        finishEditText = (EditText) view.findViewById(R.id.editTextFinish);
        mCbAccommodation = (CheckBox) view.findViewById(R.id.checkBoxAccommodation);
        mCbDiner = (CheckBox) view.findViewById(R.id.checkBoxDiner);
        distanceSeekBar = (SeekBar) view.findViewById(R.id.seekBarDistance);
        searchButton = (Button) view.findViewById(R.id.buttonSearch);
        locateButton = (ImageButton) view.findViewById(R.id.buttonYourLocation);
    }

    private void updateLocation() {
        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mLatitude = location.getLatitude();
                mLongitude = location.getLongitude();
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
            }

            @Override
            public void onProviderEnabled(String s) {
            }

            @Override
            public void onProviderDisabled(String s) {
            }
        };

        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }

    public LatLng getLocationFromAddress(String strAddress) {
        Geocoder geocoder = new Geocoder(getActivity());
        List<Address> addresses;
        LatLng latLng;

        try {
            addresses = geocoder.getFromLocationName(strAddress, 5);

            if (addresses == null || addresses.isEmpty()) {
                return null;
            }

            Address location = addresses.get(0);
            latLng = new LatLng((location.getLatitude()),
                    (location.getLongitude()));

            return latLng;
        } catch (IOException e) {
            return null;
        }
    }

    private boolean isAnyCheckboxSelected() {
        return mCbAccommodation.isChecked() || mCbDiner.isChecked();
    }

    private void startRouting() {
        LatLng position = getLocationFromAddress(finishEditText.getText().toString());
        if (position != null) {
            end = position;
            PitstopApplication.getApplicationEventBus().post(
                    new PolylineEvent(null, start, end, (distanceSeekBar.getProgress() + 1),
                            mCbDiner.isChecked(), mCbAccommodation.isChecked()));
        } else {
            Toast.makeText(getActivity(), "Nie można odnaleźć lokalizacji końcowej", Toast.LENGTH_SHORT).show();
        }
    }

    private void checkAttrs() {
        if (TextUtils.isEmpty(finishEditText.getText().toString())) {
            finishEditText.setError("Wypełnij punkt końcowy");
        } else {
            finishEditText.setError(null);
            if (isAnyCheckboxSelected()) {
                if (start != null) {
                    startRouting();
                } else {
                    LatLng position = getLocationFromAddress(startEditText.getText().toString());
                    if (position != null) {
                        start = position;
                        startRouting();
                    } else {
                        Toast.makeText(getActivity(), "Nie można odnaleźć lokalizacji początkowej", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                Toast.makeText(getActivity(), "Wybierz co najmniej jeden cel", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void search() {
        if (mIsYourLocation) {
            if (mLatitude != 0.0 && mLongitude != 0.0) {
                start = new LatLng(mLatitude, mLongitude);
                startEditText.setError(null);
                checkAttrs();
            } else {
                Toast.makeText(getActivity(), "Nie mozna rozpoznać Twojej lokalizacji", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (TextUtils.isEmpty(startEditText.getText().toString())) {
                startEditText.setError("Wypełnij punkt początkowy");
            } else {
                startEditText.setError(null);
                checkAttrs();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.choose_place_layout, container, false);
        findViews(view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addListeners();
        updateLocation();
    }

    private void addListeners() {
        distanceSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                distanceValueTextView.setText((i + 1) + getResources().getString(R.string.km));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search();
            }
        });

        locateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIsYourLocation = !mIsYourLocation;
                if (mIsYourLocation) {
                    mLastStartLocation = startEditText.getText().toString();
                    startEditText.setText("");
                    startEditText.setHint("Your location");
                    startEditText.setEnabled(false);
                } else {
                    startEditText.setHint("Start");
                    startEditText.setEnabled(true);
                    startEditText.setText(mLastStartLocation);
                }
            }
        });
    }
}

package pl.wroc.edu.pitstop2.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import pl.wroc.edu.pitstop2.R;
import pl.wroc.edu.pitstop2.maps.Route;
import pl.wroc.edu.pitstop2.model.Pitstop;
import pl.wroc.edu.pitstop2.widget.SlidingTabLayout;

/**
 * @author Jakub Skudlarski & Tomasz Trybała
 */
public class TracesFragment extends Fragment {

    private ViewPager mViewPager;
    private SlidingTabLayout mSlidingTabLayout;
    private Fragment mCurrentFragment;
    private PolylineFragment mPolylineFragment;
    private PitstopsListFragment mPitstopsListFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_traces, container,
                false);
        mViewPager = (ViewPager) v.findViewById(R.id.view_pager);
        mSlidingTabLayout = (SlidingTabLayout) v.findViewById(R.id.sliding_tabs);

        mPolylineFragment = new PolylineFragment();
        mPitstopsListFragment = new PitstopsListFragment();

        setAdapters();
        mViewPager.setCurrentItem(0);

        return v;
    }

    public void setParams(int gap, boolean isDiner, boolean isAccommodation) {
        mPolylineFragment.setParams(gap, isDiner, isAccommodation);
    }

    public void setPitstops(ArrayList<Pitstop> pitstops) {
        mPitstopsListFragment.setPitstops(pitstops);
        mSlidingTabLayout.setVisibility(View.VISIBLE);
    }

    public void setPolylinesData(Route[] routes, LatLng start, LatLng end) {
        if (mCurrentFragment == mPolylineFragment)
            mPolylineFragment.setPolylinesDeata(routes, start, end);
    }

    private class OurViewPagerAdapter extends FragmentPagerAdapter {

        public OurViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    mCurrentFragment = mPolylineFragment;
                    break;
                case 1:
                    mCurrentFragment = mPitstopsListFragment;
                    break;
            }

            return mCurrentFragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getTabName(position);
        }
    }

    private String getTabName(int position) {
        String name = "";
        switch (position) {
            case 0:
                name = this.getActivity().getString(R.string.map_tab_name);
                break;
            case 1:
                name = this.getActivity().getString(R.string.list_tab_name);
                break;
        }
        return name;
    }

    private void setAdapters() {
        OurViewPagerAdapter mViewPagerAdapter = new OurViewPagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mViewPagerAdapter);

        mSlidingTabLayout.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setBackgroundColor(getResources().getColor(R.color.apptheme_color));
        mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(android.R.color.white);
            }
        });

        mViewPagerAdapter.getItem(0);
    }
}

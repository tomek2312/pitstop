package pl.wroc.edu.pitstop2.fragments;

import android.content.ContentValues;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.wroc.edu.pitstop2.PitstopApplication;
import pl.wroc.edu.pitstop2.R;
import pl.wroc.edu.pitstop2.event.POIFoundEvent;
import pl.wroc.edu.pitstop2.maps.Route;
import pl.wroc.edu.pitstop2.model.Pitstop;
import pl.wroc.edu.pitstop2.net.HttpPost;
import pl.wroc.edu.pitstop2.net.OnRequestFinishedListener;
import pl.wroc.edu.pitstop2.net.RequestExecutor;

/**
 * @author Tomasz Trybała
 */
public class PolylineFragment extends Fragment {
    private MapView mMapView;
    private GoogleMap mMap;
    private LinearLayout mLlButtons;
    private View mBtnRed;
    private View mBtnGreen;
    private View mBtnBlue;
    private String mPolylineRed;
    private String mPolylineBlue;
    private String mPolylineGreen;
    private Polyline mPoliRed;
    private Polyline mPoliGreen;
    private Polyline mPoliBlue;
    private int mGap;
    private boolean mIsDiner;
    private boolean mIsAccommodation;
    private RequestExecutor mRequestExecutor = new RequestExecutor();

    //polylines data
    private Route[] mRoutes;
    private LatLng mStart;
    private LatLng mEnd;
    private ArrayList<Integer> mColors = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_polyline, container,
                false);

        mMapView = (MapView) v.findViewById(R.id.mapView);
        mBtnRed = v.findViewById(R.id.btnPolylineRed);
        mBtnGreen = v.findViewById(R.id.btnPolylineGreen);
        mBtnBlue = v.findViewById(R.id.btnPolylineBlue);
        mLlButtons = (LinearLayout) v.findViewById(R.id.llPolylineButtons);
        mMapView.onCreate(savedInstanceState);


        mMapView.onResume();// needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMap = mMapView.getMap();
        setColors();
        setPolylines();

        return v;
    }

    private void setPolylines() {
        clear();
        for (int i = 0; i < mRoutes.length; i++) {
            Route route = mRoutes[i];
            showPolylines(route.getPolylineOptions(),
                    route,
                    mStart,
                    mEnd,
                    mColors.get(i));
        }
    }

    private void parseMessage(String message) {
        final JSONObject json;
        ArrayList<Pitstop> pitstops = new ArrayList<>();
        try {
            json = new JSONObject(message);
            final JSONArray entities = json.getJSONArray("entities");
            for (int i = 0; i < entities.length(); i++) {
                JSONObject object = entities.getJSONObject(i);
                Pitstop pitstop = new Pitstop();

                if (object.has("name")) {
                    pitstop.setName(object.getString("name"));
                }

                if (object.has("description")) {
                    pitstop.setDescription(object.getString("description"));
                }

                if (object.has("place_type")) {
                    pitstop.setPlaceType(object.getString("place_type"));
                }

                if (object.has("latitude")) {
                    pitstop.setLatitude(object.getDouble("latitude"));
                }

                if (object.has("longitude")) {
                    pitstop.setLongitude(object.getDouble("longitude"));
                }

                if (object.has("phone")) {
                    pitstop.setPhone(object.getString("phone"));
                }

                if (object.has("url")) {
                    pitstop.setUrl(object.getString("url"));
                }

                if (object.has("type")) {
                    JSONObject types = object.getJSONObject("type");
                    pitstop.setType(types.getString("name"));
                }

                if (object.has("hours")) {
                    JSONObject time = object.getJSONObject("hours");
                    pitstop.setTimeOpen(time.getString("open"));
                    pitstop.setTimeClose(time.getString("close"));
                }

                if (pitstop.getPlaceType().equals("diner")) {
                    if (object.has("couisine")) {
                        JSONArray cousines = object.getJSONArray("couisine");
                        JSONObject cousine = cousines.getJSONObject(0);
                        pitstop.setCouisine(cousine.getString("name"));
                    }
                } else {
                    if (object.has("stars")) {
                        pitstop.setStars(object.getInt("stars"));
                    }
                }

                pitstops.add(pitstop);
            }

            PitstopApplication.getApplicationEventBus().post(
                    new POIFoundEvent(pitstops));

            for(Pitstop pitstop: pitstops){
                animate(new LatLng(pitstop.getLatitude(),
                        pitstop.getLongitude()), pitstop.getName());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListeners();
    }

    private void setListeners() {
        mBtnRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendPostOnServer(mPolylineRed);
            }
        });

        mBtnBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendPostOnServer(mPolylineBlue);
            }
        });

        mBtnGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendPostOnServer(mPolylineGreen);
            }
        });

    }

    private void animate(final LatLng target, String title) {
        final Marker marker = mMap.addMarker(new MarkerOptions()
                .position(target)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pitstop_marker))
                .title(title));

        dropPinEffect(marker);
    }

    private void dropPinEffect(final Marker marker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final long duration = 1000;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = Math.max(
                        1 - interpolator.getInterpolation((float) elapsed
                                / duration), 0);
                marker.setAnchor(0.5f, 1.0f + 14 * t);

                if (t > 0.0) {
                    // Post again 15ms later.
                    handler.postDelayed(this, 15);
                } else {
                    marker.showInfoWindow();

                }
            }
        });
    }

    private void setColors() {
        mColors.add(Color.RED);
        mColors.add(Color.BLUE);
        mColors.add(Color.GREEN);
    }

    private void sendPostOnServer(final String polyline) {
        final OnRequestFinishedListener<String> listener = new OnRequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(int responseCode, String message, boolean canceled) {
                if (responseCode == 200) {
                    mLlButtons.setVisibility(View.GONE);
                    if (mPolylineRed != null && !mPolylineRed.equals(polyline)) {
                        mPoliRed.remove();
                    }

                    if (mPolylineBlue != null && !mPolylineBlue.equals(polyline)) {
                        mPoliBlue.remove();
                    }

                    if (mPolylineGreen != null && !mPolylineGreen.equals(polyline)) {
                        mPoliGreen.remove();
                    }
                    parseMessage(message);

                } else {
                    Log.e("dupa", "code: " + responseCode + ", message: " + message);
                }
            }
        };

        final ContentValues contentValues = new ContentValues();
        contentValues.put("polyline", polyline);
        contentValues.put("distance_gap", mGap);
        contentValues.put("dinner", mIsDiner ? "true" : "false");
        contentValues.put("accommodation", mIsAccommodation ? "true" : "false");

        HttpPost<String> postRequest = new HttpPost<>(
                mRequestExecutor,
                false,
                getResources().getString(R.string.server_host),
                contentValues,
                new HttpPost.ServerResponseParser<String>() {
                    @Override
                    public String parse(String message) {
                        return message;
                    }
                },
                listener
        );

        postRequest.execute();
    }

    public void clear() {
        if (mMap != null) {
            mMap.clear();
        }
        mBtnRed.setVisibility(View.GONE);
        mBtnGreen.setVisibility(View.GONE);
        mBtnBlue.setVisibility(View.GONE);
        mPoliRed = null;
        mPoliBlue = null;
        mPoliGreen = null;
        mPolylineRed = null;
        mPolylineGreen = null;
        mPolylineBlue = null;
    }

    public void setParams(int gap, boolean isDiner, boolean isAccommodation) {
        mGap = gap;
        mIsDiner = isDiner;
        mIsAccommodation = isAccommodation;
    }

    public void setPolylinesDeata(Route[] routes, LatLng start, LatLng end) {
        this.mRoutes = routes;
        this.mStart = start;
        this.mEnd = end;
    }

    public void showPolylines(PolylineOptions mPolyOptions, Route route, final LatLng start, final LatLng end, int color) {
        switch (color) {
            case Color.RED:
                mPolylineRed = route.getPolyline();
                mBtnRed.setVisibility(View.VISIBLE);
                mPoliRed = mMap.addPolyline(new PolylineOptions()
                        .color(color)
                        .width(5)
                        .addAll(mPolyOptions.getPoints()));
                break;
            case Color.BLUE:
                mPolylineBlue = route.getPolyline();
                mBtnBlue.setVisibility(View.VISIBLE);
                mPoliBlue = mMap.addPolyline(new PolylineOptions()
                        .color(color)
                        .width(5)
                        .addAll(mPolyOptions.getPoints()));
                break;
            case Color.GREEN:
                mPolylineGreen = route.getPolyline();
                mBtnGreen.setVisibility(View.VISIBLE);
                mPoliGreen = mMap.addPolyline(new PolylineOptions()
                        .color(color)
                        .width(5)
                        .addAll(mPolyOptions.getPoints()));
                break;
        }

        mLlButtons.setVisibility(View.VISIBLE);

        // Start marker
        MarkerOptions options = new MarkerOptions();
        options.position(start);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_start));
        mMap.addMarker(options);

        // End marker
        options = new MarkerOptions();
        options.position(end);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_finish));
        mMap.addMarker(options);

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {

            @Override
            public void onCameraChange(CameraPosition arg0) {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(start);
                builder.include(end);
                LatLngBounds bounds = builder.build();

                int padding = (int) getResources().getDimension(R.dimen.padding_xlarge);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                mMap.moveCamera(cameraUpdate);
                mMap.animateCamera(cameraUpdate);

                mMap.setOnCameraChangeListener(null);

            }
        });

        mMap.moveCamera(CameraUpdateFactory.zoomIn());
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}

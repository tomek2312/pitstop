package pl.wroc.edu.pitstop2.fragments;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.eventbus.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.wroc.edu.pitstop2.BundleConstants;
import pl.wroc.edu.pitstop2.PitstopActivity;
import pl.wroc.edu.pitstop2.PitstopApplication;
import pl.wroc.edu.pitstop2.R;
import pl.wroc.edu.pitstop2.event.NetErrorEvent;
import pl.wroc.edu.pitstop2.model.Pitstop;
import pl.wroc.edu.pitstop2.net.HttpPost;
import pl.wroc.edu.pitstop2.net.OnRequestFinishedListener;
import pl.wroc.edu.pitstop2.net.RequestExecutor;

/**
 * @author Tomasz Trybała
 */
public class AllPitstopsFragment extends Fragment {
    /*package*/ static class PitstopListAdapter extends ArrayAdapter<Pitstop> {
        private final Context mContext;
        private final ArrayList<Pitstop> mData;
        private final int mLayout;


        private class ViewHolder {
            public TextView mPitstopName;
            public ImageView mPitstopImage;
        }

        private void getImage(final String type, final ImageView imageView) {
            AsyncTask<Void, Void, Bitmap> bmpTask = new AsyncTask<Void, Void, Bitmap>() {
                @Override
                protected Bitmap doInBackground(Void... params) {
                    return BitmapFactory.decodeResource(mContext.getResources(),
                            type.equals("diner") ?
                                    R.drawable.ic_eat : R.drawable.ic_sleep);
                }

                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    imageView.setImageBitmap(bitmap);
                }
            };
            bmpTask.execute();
        }

        public PitstopListAdapter(Context context, ArrayList<Pitstop> data, int layout) {
            super(context, layout, data);

            mContext = context;
            mData = data;
            mLayout = layout;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(mContext);
                convertView = inflater.inflate(mLayout, viewGroup, false);
                viewHolder = new ViewHolder();

                //noinspection ConstantConditions
                viewHolder.mPitstopName = (TextView) convertView.findViewById(R.id.txtvAllPitstopsRow);
                viewHolder.mPitstopImage = (ImageView) convertView.findViewById(R.id.imgvAllPitstopsRow);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final Pitstop pitstop = mData.get(position);
            viewHolder.mPitstopName.setText(pitstop.getName());
            getImage(pitstop.getPlaceType(), viewHolder.mPitstopImage);

            return convertView;
        }
    }

    private ListView mListView;
    private ProgressBar mProgressBar;
    private PitstopActivity mActivity;
    private PitstopListAdapter mAdapter;
    private ArrayList<Pitstop> mPitstops = new ArrayList<>();
    private RequestExecutor mRequestExecutor = new RequestExecutor();
    private int mIndex = 0;

    private void setAdapter() {
        mAdapter = new PitstopListAdapter(PitstopApplication.getAppContext(),
                mPitstops, R.layout.all_pitstops_list_row);
        mListView.setAdapter(mAdapter);
    }

    private void setListeners() {
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle arguments = new Bundle();
                arguments.putSerializable(BundleConstants.PITSTOP, mPitstops.get(i));
                arguments.putString(BundleConstants.IS_MAP, "");
                PitstopFragment fragment = new PitstopFragment();
                fragment.setArguments(arguments);
                mActivity.setFragment(fragment);
            }
        });
    }

    private void findViews(View root) {
        mListView = (ListView) root.findViewById(R.id.listView);
        mProgressBar = (ProgressBar) root.findViewById(R.id.progressBar);
    }

    private void parseMessage(String message) {
        final JSONObject json;
        try {
            json = new JSONObject(message);
            final JSONArray entities = json.getJSONArray("entities");
            for (int i = 0; i < entities.length(); i++) {
                JSONObject object = entities.getJSONObject(i);
                Pitstop pitstop = new Pitstop();

                if (object.has("name")) {
                    pitstop.setName(object.getString("name"));
                }

                if (object.has("description")) {
                    pitstop.setDescription(object.getString("description"));
                }

                if (object.has("place_type")) {
                    pitstop.setPlaceType(object.getString("place_type"));
                }

                if (object.has("latitude")) {
                    pitstop.setLatitude(object.getDouble("latitude"));
                }

                if (object.has("longitude")) {
                    pitstop.setLongitude(object.getDouble("longitude"));
                }

                if (object.has("phone")) {
                    pitstop.setPhone(object.getString("phone"));
                }

                if (object.has("url")) {
                    pitstop.setUrl(object.getString("url"));
                }

                if (object.has("type")) {
                    JSONObject types = object.getJSONObject("type");
                    pitstop.setType(types.getString("name"));
                }

                if (object.has("hours")) {
                    JSONObject time = object.getJSONObject("hours");
                    pitstop.setTimeOpen(time.getString("open"));
                    pitstop.setTimeClose(time.getString("close"));
                }

                if (pitstop.getPlaceType().equals("diner")) {
                    if (object.has("couisine")) {
                        JSONArray cousines = object.getJSONArray("couisine");
                        JSONObject cousine = cousines.getJSONObject(0);
                        pitstop.setCouisine(cousine.getString("name"));
                    }
                } else {
                    if (object.has("stars")) {
                        pitstop.setStars(object.getInt("stars"));
                    }
                }

                mPitstops.add(pitstop);
                mAdapter.notifyDataSetChanged();
            }
        } catch (JSONException e) {
            Toast.makeText(PitstopApplication.getAppContext(),
                    "Niepoprawne dane z serwera!",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void sendPostOnServer() {
        final OnRequestFinishedListener<String> listener = new OnRequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(int responseCode, String message, boolean canceled) {
                mProgressBar.setVisibility(View.INVISIBLE);
                if (responseCode == 200) {
                    parseMessage(message);
                } else {
                    Toast.makeText(PitstopApplication.getAppContext(),
                            "Empty data!",
                            Toast.LENGTH_SHORT).show();
                }
            }
        };

        final ContentValues contentValues = new ContentValues();
        contentValues.put("index", mIndex);

        HttpPost<String> postRequest = new HttpPost<>(
                mRequestExecutor,
                false,
                getResources().getString(R.string.server_host_all),
                contentValues,
                new HttpPost.ServerResponseParser<String>() {
                    @Override
                    public String parse(String message) {
                        return message;
                    }
                },
                listener
        );

        mProgressBar.setVisibility(View.VISIBLE);
        postRequest.execute();
    }

    private void prepareLayout() {
        mActivity.getSupportActionBar().
                setTitle(getResources().
                        getString(R.string.activity_all_pitstops_label));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_all_pitstops, container, false);
        findViews(root);

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareLayout();
        setAdapter();
        setListeners();
        sendPostOnServer();
    }

    @Override
    public void onStart() {
        super.onStart();
        PitstopApplication.getApplicationEventBus().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        PitstopApplication.getApplicationEventBus().unregister(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (PitstopActivity) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Subscribe
    public void setNetError(final NetErrorEvent event) {
        mProgressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(PitstopApplication.getAppContext(),
                "Check your internet connection!",
                Toast.LENGTH_SHORT).show();
    }
}

package pl.wroc.edu.pitstop2.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import pl.wroc.edu.pitstop2.BundleConstants;
import pl.wroc.edu.pitstop2.MainActivity;
import pl.wroc.edu.pitstop2.PitstopActivity;
import pl.wroc.edu.pitstop2.PitstopApplication;
import pl.wroc.edu.pitstop2.R;
import pl.wroc.edu.pitstop2.model.Pitstop;

/**
 * @author Tomasz Trybała
 */
public class PitstopFragment extends Fragment {
    //region views
    private TextView mTxtvName;
    private TextView mTxtvType;
    private TextView mTxtvPhone;
    private TextView mTxtvUrl;
    private TextView mTxtvHours;
    private TextView mTxtvDesc;
    private TextView mTxtvExtraValue;
    private TextView mTxtvExtraTitle;
    private Button mBtnChange;
    private Button mBtnNavigate;
    private MapView mMapView;
    private ScrollView mSvVontent;
    //endregion

    //region variables
    private MarkerOptions mMarkerOptions = new MarkerOptions();
    private boolean mIsMap;
    private Pitstop mPitstop;
    private GoogleMap mMap;
    //endregion

    private void loadPitstop() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            if (arguments.containsKey(BundleConstants.PITSTOP)) {
                mPitstop = (Pitstop) arguments.getSerializable(BundleConstants.PITSTOP);

                mMarkerOptions = new MarkerOptions()
                        .title(mPitstop.getName())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pitstop_marker));

                initViews(arguments.containsKey(BundleConstants.IS_MAP));
            }
        }
    }

    private void findViews(View root) {
        mTxtvName = (TextView) root.findViewById(R.id.txtvPitstopName);
        mTxtvType = (TextView) root.findViewById(R.id.txtvPitstopType);
        mTxtvPhone = (TextView) root.findViewById(R.id.txtvPitstopPhone);
        mTxtvDesc = (TextView) root.findViewById(R.id.txtvPitstopDesc);
        mTxtvUrl = (TextView) root.findViewById(R.id.txtvPitstopUrl);
        mTxtvHours = (TextView) root.findViewById(R.id.txtvPitstopHours);
        mTxtvExtraTitle = (TextView) root.findViewById(R.id.txtvPitstopExtraTitle);
        mTxtvExtraValue = (TextView) root.findViewById(R.id.txtvPitstopExtraValue);
        mBtnChange = (Button) root.findViewById(R.id.btnMapChange);
        mBtnNavigate = (Button) root.findViewById(R.id.btnMapNavigate);
        mMapView = (MapView) root.findViewById(R.id.mapView);
        mSvVontent = (ScrollView) root.findViewById(R.id.svContent);
    }

    private void initViews(boolean isMap) {
        mTxtvName.setText(mPitstop.getName() != null ? mPitstop.getName() : "Brak danych");
        mTxtvType.setText(mPitstop.getType() != null ? mPitstop.getType() : "Brak danych");
        mTxtvPhone.setText(mPitstop.getPhone() != null ? mPitstop.getPhone() : "Brak danych");
        mTxtvUrl.setText(mPitstop.getUrl() != null ? mPitstop.getUrl() : "Brak danych");
        mTxtvHours.setText(mPitstop.getTimeOpen() != null && mPitstop.getTimeClose() != null ?
                mPitstop.getTimeOpen() + " - " + mPitstop.getTimeClose() :
                "Brak danych");
        mTxtvDesc.setText(mPitstop.getDescription() != null ? mPitstop.getDescription() : "Brak danych");
        if (mPitstop.getPlaceType().equals("diner")) {
            mTxtvExtraTitle.setText("Kuchnia");
            mTxtvExtraValue.setText(mPitstop.getCouisine() != null ? mPitstop.getCouisine() : "Brak danych");
        } else {
            mTxtvExtraTitle.setText("Ocena");
            mTxtvExtraValue.setText(mPitstop.isStars() ? "" + mPitstop.getStars() : "Brak danych");
        }

        mBtnChange.setVisibility(isMap ? View.VISIBLE : View.GONE);
    }

    private void animateMap() {
        mMap.clear();
        mMap.addMarker(mMarkerOptions.position(new LatLng(mPitstop.getLatitude(),
                mPitstop.getLongitude())));

        if (mMap.getCameraPosition().zoom < 15) {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(mPitstop.getLatitude(),
                            mPitstop.getLongitude())).zoom(15).build();
            mMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
        }
    }

    private void setListeners() {
        mBtnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mIsMap) {
                    mMapView.setVisibility(View.VISIBLE);
                    mSvVontent.setVisibility(View.GONE);
                    animateMap();
                    mBtnChange.setText("Informacje");
                } else {
                    mMapView.setVisibility(View.GONE);
                    mSvVontent.setVisibility(View.VISIBLE);
                    mBtnChange.setText("Mapa");
                }

                mIsMap = !mIsMap;
            }
        });

        mBtnNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(
                        "google.navigation:q=" + mPitstop.getLatitude() +
                                "," + mPitstop.getLongitude());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }

    private void initMap(Bundle savedInstanceState) {
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        try {
            MapsInitializer.initialize(PitstopApplication.getAppContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMap = mMapView.getMap();
    }

    private void setToolbar() {
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.fragment_pitstop_label));
        } else {
            ((PitstopActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.fragment_pitstop_label));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_pitstop, container, false);
        findViews(root);
        initMap(savedInstanceState);

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadPitstop();
        setListeners();
        setToolbar();
    }
}

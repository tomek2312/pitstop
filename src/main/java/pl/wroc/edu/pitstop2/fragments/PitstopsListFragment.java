package pl.wroc.edu.pitstop2.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import pl.wroc.edu.pitstop2.BundleConstants;
import pl.wroc.edu.pitstop2.MainActivity;
import pl.wroc.edu.pitstop2.PitstopApplication;
import pl.wroc.edu.pitstop2.R;
import pl.wroc.edu.pitstop2.model.Pitstop;


public class PitstopsListFragment extends Fragment {
    /*package*/ static class PitstopListAdapter extends ArrayAdapter<Pitstop> {
        private final Context mContext;
        private final ArrayList<Pitstop> mData;
        private final int mLayout;


        private class ViewHolder {
            public TextView mPitstopName;
            public ImageView mPitstopImage;
        }

        private void getImage(final String type, final ImageView imageView) {
            AsyncTask<Void, Void, Bitmap> bmpTask = new AsyncTask<Void, Void, Bitmap>() {
                @Override
                protected Bitmap doInBackground(Void... params) {
                    return BitmapFactory.decodeResource(mContext.getResources(),
                            type.equals("diner") ?
                                    R.drawable.ic_eat : R.drawable.ic_sleep);
                }

                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    imageView.setImageBitmap(bitmap);
                }
            };
            bmpTask.execute();
        }

        public PitstopListAdapter(Context context, ArrayList<Pitstop> data, int layout) {
            super(context, layout, data);

            mContext = context;
            mData = data;
            mLayout = layout;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(mContext);
                convertView = inflater.inflate(mLayout, viewGroup, false);
                viewHolder = new ViewHolder();

                //noinspection ConstantConditions
                viewHolder.mPitstopName = (TextView) convertView.findViewById(R.id.txtvAllPitstopsRow);
                viewHolder.mPitstopImage = (ImageView) convertView.findViewById(R.id.imgvAllPitstopsRow);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final Pitstop pitstop = mData.get(position);
            viewHolder.mPitstopName.setText(pitstop.getName());
            getImage(pitstop.getPlaceType(), viewHolder.mPitstopImage);

            return convertView;
        }
    }

    private ListView mListView;
    private PitstopListAdapter mAdapter;
    private ArrayList<Pitstop> mData = new ArrayList<>();
    private MainActivity mActivity;

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root =  inflater.inflate(R.layout.fragment_pitstops_list,null);
        findViews(root);

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAdapter();
        setListeners();
    }

    private void findViews(View root){
        mListView = (ListView) root.findViewById(R.id.listViewPoints);
    }

    private void setAdapter() {
        mAdapter = new PitstopListAdapter(PitstopApplication.getAppContext(),
                mData, R.layout.all_pitstops_list_row);
        mListView.setAdapter(mAdapter);
    }

    public void setPitstops(ArrayList<Pitstop> pitstops){
        mData.clear();
        mData.addAll(pitstops);
        mAdapter.notifyDataSetChanged();
    }

    private void setListeners() {
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle arguments = new Bundle();
                arguments.putSerializable(BundleConstants.PITSTOP, mData.get(i));
                PitstopFragment fragment = new PitstopFragment();
                fragment.setArguments(arguments);
                mActivity.setFragment(fragment);
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (MainActivity) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }
}

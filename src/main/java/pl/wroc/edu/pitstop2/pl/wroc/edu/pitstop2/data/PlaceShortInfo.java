package pl.wroc.edu.pitstop2.pl.wroc.edu.pitstop2.data;

/**
 * @author Jakub Skudlarski
 */
public class PlaceShortInfo {

    private String name;
    private String info;
    private double rating;
    private double distanceFromPath;
    //TODO more?


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double getDistanceFromPath() {
        return distanceFromPath;
    }

    public void setDistanceFromPath(double distanceFromPath) {
        this.distanceFromPath = distanceFromPath;
    }
}

package pl.wroc.edu.pitstop2;

/**
 *@author Tomasz Trybała
 */
public enum MainType {
    CHOOSE_PLACE("Select trace"),
    POLYLINE("Trace");

    private String mLabel;

    private MainType(String label){
        mLabel = label;
    }

    public String getLabel(){return mLabel;}
}

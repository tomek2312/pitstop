package pl.wroc.edu.pitstop2.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author Tomasz Trybała
 */
public class Pitstop implements Serializable {
    private int mId;
    @SerializedName("name")
    private String mName;
    @SerializedName("latitude")
    private double mLatitude;
    @SerializedName("longitude")
    private double mLongitude;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("url")
    private String mUrl;
    @SerializedName("phone")
    private String mPhone;
    private int mStatus;
    @SerializedName("stars")
    private int mStars;
    @SerializedName("start_time")
    private String mTimeOpen;
    @SerializedName("end_time")
    private String mTimeClose;
    @SerializedName("pitstop_type")
    private String mType;
    private String mPlaceType;
    @SerializedName("cousine_type")
    private String mCouisine;
    private boolean mIsStars;

    public Pitstop() {
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double mLongitude) {
        this.mLongitude = mLongitude;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    public int getStatus() {
        return mStatus;
    }

    public void setStatus(int mStatus) {
        this.mStatus = mStatus;
    }

    public int getStars() {
        return mStars;
    }

    public void setStars(int mStars) {
        mIsStars = true;
        this.mStars = mStars;
    }

    public boolean isStars(){
        return mIsStars;
    }

    public String getTimeOpen() {
        return mTimeOpen;
    }

    public void setTimeOpen(String mTimeOpen) {
        this.mTimeOpen = mTimeOpen;
    }

    public String getTimeClose() {
        return mTimeClose;
    }

    public void setTimeClose(String mTimeClose) {
        this.mTimeClose = mTimeClose;
    }

    public String getType() {
        return mType;
    }

    public void setType(String mType) {
        this.mType = mType;
    }

    public String getPlaceType() {
        return mPlaceType;
    }

    public void setPlaceType(String mPlaceType) {
        this.mPlaceType = mPlaceType;
    }

    public String getCouisine() {
        return mCouisine;
    }

    public void setCouisine(String mCouisine) {
        this.mCouisine = mCouisine;
    }
}

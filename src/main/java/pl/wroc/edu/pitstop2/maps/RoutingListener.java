package pl.wroc.edu.pitstop2.maps;

public interface RoutingListener {
    public void onRoutingFailure();

    public void onRoutingStart();

    public void onRoutingSuccess(Route[] route);
}

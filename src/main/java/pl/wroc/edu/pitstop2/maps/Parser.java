package pl.wroc.edu.pitstop2.maps;

public interface Parser {
    public Route[] parse();
}

package pl.wroc.edu.pitstop2.net;

import android.content.ContentValues;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.common.io.Closer;
import com.google.gson.Gson;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import pl.wroc.edu.pitstop2.model.Pitstop;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Marcin Przepiórkowski
 *
 * This is a generic HTTP POST request. This class should be used to construct specialized
 * POST requests.
 */
public class HttpPost<T> {
    /**
     * The interface of the server response parser.
     */
    public static interface ServerResponseParser<T> {
        /**
         * Invoked to parse server response.
         *
         * @param json  server response in the json format
         * @return      parsed server response
         */
        T parse(String json);
    }

    private boolean mUseSsl;
    private RequestExecutor mRequestExecutor;
    private String mRequestUrl;
    private ContentValues mPostParams;
    private Pitstop mPitstop;
    private ServerResponseParser<T> mParser;
    private OnRequestFinishedListener<T> mExecutionListener;

    /**
     * Default PostRequest constructor.
     *
     * @param requestUrl            http request url
     * @param postParams            request input parameters
     * @param parser                server response parser
     * @param executionListener     execution listener
     */
    public HttpPost(RequestExecutor executor,
                    boolean useSsl,
                    String requestUrl,  ContentValues postParams,
                     ServerResponseParser<T> parser,
                     OnRequestFinishedListener<T> executionListener) {
        checkNotNull(postParams, "postParams cannot be null");
        checkNotNull(requestUrl, "requestUrl cannot be null");
        checkArgument(!requestUrl.isEmpty(), "requestUrl cannot be empty");

        mRequestUrl = requestUrl;
        mRequestExecutor = executor;
        mPostParams = postParams;
        mParser = parser;
        mExecutionListener = executionListener;
    }

    public HttpPost(RequestExecutor executor,
                    boolean useSsl,
                    String requestUrl,  Pitstop pitstop,
                    ServerResponseParser<T> parser,
                    OnRequestFinishedListener<T> executionListener) {
        checkNotNull(requestUrl, "requestUrl cannot be null");
        checkArgument(!requestUrl.isEmpty(), "requestUrl cannot be empty");

        mRequestUrl = requestUrl;
        mRequestExecutor = executor;
        mPitstop = pitstop;
        mParser = parser;
        mExecutionListener = executionListener;
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }
        return result.toString();
    }

    /**
     * Executes this request asynchronously in the RequestExecutor. When the execution finishes,
     * executionListener is invoked (if it's not null). When an error occurs, a NetErrorEvent instance
     * is posted on the application event bus.
     *
     * @return  the task id, that can be used to cancel it later
     */
    public String execute() {
        final String urlParams = NetUtils.buildPostParamsString(mPostParams);

        CancelableTask<RequestResult> requestTask = new CancelableTask<RequestResult>() {
            @Override
            public RequestResult call() throws Exception {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

                HttpURLConnection connection = null;
                int responseCode = Integer.MAX_VALUE;
                String responseMessage = "";

                try {
                    URL serviceUrl = new URL(mRequestUrl);
                    if (mUseSsl) {
                        connection = (HttpsURLConnection) serviceUrl.openConnection();
                    } else {
                        connection = (HttpURLConnection) serviceUrl.openConnection();
                    }
                    connection.setConnectTimeout(Constants.CONNECTION_TIMEOUT);
                    connection.setReadTimeout(Constants.READ_TIMEOUT);
                    connection.setRequestMethod("POST");
                    connection.setDoOutput(true);

                    Closer closer = Closer.create();
                    try {
                        DataOutputStream out = closer.register(new DataOutputStream(connection.getOutputStream()));
                        out.writeBytes(urlParams);
                        out.flush();
                    } catch (Throwable e) {
                        throw closer.rethrow(e);
                    } finally {
                        closer.close();
                    }

                    responseCode = connection.getResponseCode();

                    if (responseCode < 400) {
                        closer = Closer.create();
                        try {
                            InputStream is = closer.register(connection.getInputStream());
                            responseMessage = NetUtils.getRequestResponse(is);
                            is.close();
                        } catch (Throwable e) {
                           // noinspection ThrowableResultOfMethodCallIgnored
                            closer.rethrow(e);
                        } finally {
                            closer.close();
                        }
                    } else {
                        closer = Closer.create();
                        try {
                            InputStream is = connection.getErrorStream();
                            responseMessage = NetUtils.getRequestResponse(is);
                            is.close();
                        } catch (Throwable e) {
                            //noinspection ThrowableResultOfMethodCallIgnored
                            closer.rethrow(e);
                        } finally {
                            closer.close();
                        }
                    }
                }
                finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                }

                if (mExecutionListener != null && mParser != null) {
                    try {
                        final int code = responseCode;
                        final T response = mParser.parse(responseMessage);

                        if (response == null) {
                            throw new Exception("cannot parse the response");
                        }

                        Handler handler = new Handler(Looper.getMainLooper());
                        handler.post(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        mExecutionListener.onRequestFinished(code, response, false);
                                    }
                                }
                        );
                    } catch (Exception e) {
                        System.out.println("EXCEPTION: " + e.getMessage());
                    }
                }

                return new RequestResult(mRequestUrl, responseMessage, mRequestUrl, responseCode);
            }

            @Override
            public void cancel() {
            }
        };

        return mRequestExecutor.execute(requestTask, mRequestUrl);
    }

    public String executePitstop() {
//        final String urlParams = NetUtils.buildPostParamsString(mPostParams);

        CancelableTask<RequestResult> requestTask = new CancelableTask<RequestResult>() {
            @Override
            public RequestResult call() throws Exception {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

                HttpURLConnection connection = null;
                int responseCode = Integer.MAX_VALUE;
                String responseMessage = "";

                try {
                    URL serviceUrl = new URL(mRequestUrl);
                    if (mUseSsl) {
                        connection = (HttpsURLConnection) serviceUrl.openConnection();
                    } else {
                        connection = (HttpURLConnection) serviceUrl.openConnection();
                    }
                    connection.setConnectTimeout(Constants.CONNECTION_TIMEOUT);
                    connection.setReadTimeout(Constants.READ_TIMEOUT);
                    connection.setRequestMethod("POST");
                    connection.setDoOutput(true);

                    Closer closer = Closer.create();
                    try {
                        DataOutputStream out = closer.register(new DataOutputStream(connection.getOutputStream()));
                        String gson = new Gson().toJson(mPitstop);
                        out.writeBytes(gson);
                        out.flush();
                    } catch (Throwable e) {
                        throw closer.rethrow(e);
                    } finally {
                        closer.close();
                    }

                    responseCode = connection.getResponseCode();

                    if (responseCode < 400) {
                        closer = Closer.create();
                        try {
                            InputStream is = closer.register(connection.getInputStream());
                            responseMessage = NetUtils.getRequestResponse(is);
                            is.close();
                        } catch (Throwable e) {
                            // noinspection ThrowableResultOfMethodCallIgnored
                            closer.rethrow(e);
                        } finally {
                            closer.close();
                        }
                    } else {
                        closer = Closer.create();
                        try {
                            InputStream is = connection.getErrorStream();
                            responseMessage = NetUtils.getRequestResponse(is);
                            is.close();
                        } catch (Throwable e) {
                            //noinspection ThrowableResultOfMethodCallIgnored
                            closer.rethrow(e);
                        } finally {
                            closer.close();
                        }
                    }
                }
                finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                }

                if (mExecutionListener != null && mParser != null) {
                    try {
                        final int code = responseCode;
                        final T response = mParser.parse(responseMessage);

                        if (response == null) {
                            throw new Exception("cannot parse the response");
                        }

                        Handler handler = new Handler(Looper.getMainLooper());
                        handler.post(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        mExecutionListener.onRequestFinished(code, response, false);
                                    }
                                }
                        );
                    } catch (Exception e) {
                        System.out.println("EXCEPTION: " + e.getMessage());
                    }
                }

                return new RequestResult(mRequestUrl, responseMessage, mRequestUrl, responseCode);
            }

            @Override
            public void cancel() {
            }
        };

        return mRequestExecutor.execute(requestTask, mRequestUrl);
    }
}